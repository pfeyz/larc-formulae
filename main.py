
"""

TODO: Come up with two different names for the 3-tuple of (uid, speaker,
list-of-words), and the list-of-words itself. Right now I'm using "utterance" to
refer to both interchangeably and it's confusing.

"""

from typing import Iterator, Tuple, Dict, Callable, List, TypeVar
from collections import Counter
from talkbank_parser import MorParser, MorToken
from testngrams import*

def identity(val):
    "An identity function; returns its input"

    return val

def constant(val):
    "Returns a constant function that returns `val` regardless of its input."

    return lambda _: val

# type definitions

illegal = {'one two': False,
           'two three': False,
           'a b': False,
           'b c': False,
           'f g': False,
           'd e': False,
           'c d': False,
           'e f': False,
           'uh oh': False,
           'oh oh': False,
           'um um': False,
           'um a': False,
           'beep beep': False,
           'tweet tweet': False,
           'night moon': False,
           'night night': False,
           'old MacDon': False,
           'MacDon had': False,
           'ring around': False,
           'around the': False,
           'the rosey': False,
           'J P': False,
           'New Investigator': False,
           'Tot Time': False,
           'one two three': False,
           'two three four': False,
           'three four five': False,
           'four five six': False,
           'five six seven': False,
           'six seven eight': False,
           'seven eight nine': False,
           'eight nine ten': False,
           'nine ten eleven': False,
           'old MacDon had': False,
           'MacDon had a': False,
           'a b c': False,
           'd e f': False,
           'e f g': False,
           'e f g': False,
           'b c d': False,
           'c d e': False,
           'h i j': False,
           'f g h': False,
           'ring around the': False,
           'around the rosey': False,
           'pop goes the': False,
           'quack quack quack': False}
Speaker = str
Group = str
Uid = str
Utterance = List[MorToken]
F = TypeVar('F')

def generate_chunks(utterances,                   # type: Iterator[Tuple[Uid, Speaker, Utterance]]
                    chunker,                      # type: Callable[[Utterance], List[Tuple[F, ...]]]
                    utterance_filter,              # type: Callable[[Utterance], bool]
                    speaker_grouper=identity,     # type: Callable[[Speaker], Group]
                    existing = {}
):
    # type (...) -> Dict[str, List[Tuple[F, ...]]]

    """Generate the per-speaker formulaic chunks from a sequence of utterances.

    A "chunk" here is a tuple of elements, most likely strings. The `chunker`
    function generates a list of chunks given a list of MorTokens, aka an
    Utterance.

    In the case of our chunks being trigrams of words, for example, the chunker
    will be a function that takes a list of MorTokens and returns a list of the
    ngrams of the wordforms in that utterance. Each chunk will be a 3-tuple of
    strings: ("I", "want", "to").

    `generate_chunks` returns a dictionary that maps from speaker names (which
    can be renamed by the `speaker_grouper` function) to a list of all chunks
    observed by that speaker in the passed-in utterances.

    Args:

        utterances: An iterator of 3-tuples, (uid, speaker, utterance).

        chunker: A function which turns an utterance into a list of formulaic
          chunks.

        speaker_grouper: A function which translates a speaker name into a group
          name. For example:

              'CHI' -> 'child'
              'MOT' -> 'adult'
              'INV' -> 'adult'.

          If `speaker_grouper` returns None, the utterance will be excluded from
          the returned chunks. Defaults to identity, aka, no change

        utterance_filter: A function that accepts an utterance and returns a
          boolean. False indicates the utterance should be excluded from
          chunking. Defaults to including all utterances that are not rejected
          by `speaker_grouper`.

    Returns:
        a dictionary of mappings from speaker names (as translated by
        `speaker_grouper`) to chunks generated from that speaker's utterances.

    """
    #i = 0
    #j = 0
    grouped = existing  # type: Dict[str, List[Tuple[F, ...]]]
    for _, speaker, tokens in utterances:
        group = speaker_grouper(speaker)
        word_forms = [x.word for x in tokens]
        test = utterance_filter(word_forms)
        '''
        if test:
            i+=1
        else:
            j+=1
        '''
        if group and test:
            tidied_tokens = clean(word_forms)
            try:
                grouped[group].extend(chunker(tidied_tokens))
            except KeyError:
                grouped[group] = chunker(tidied_tokens)
    #print("okay:", i, "not okay: ", j)
    return grouped

def example_usage():
    parser = MorParser()
    utterances = parser.parse("/Users/Selulel/Dropbox/LARC/Code/Valian/01a.xml")
    chunks = generate_chunks(utterances, chunker=(lambda x: createngrams(x, 2)), utterance_filter= (lambda x: filterall(x)))
    return chunks

def generated(transcript, size, earlier_dict = {}):
    parser = MorParser()
    utterances = parser.parse(transcript)
    chunks = generate_chunks(utterances, chunker=(lambda x: createngrams(x, size)), utterance_filter= (lambda x: filterall(x)), existing=earlier_dict)
    return chunks

    #chunks = generate_chunks(utterances,
    #                         chunker=...)  # some function that takes an
    # utterance and returns a list of
    # formulaic chunks, a like a list of
    # n-gram tuples for example.
    #child_counts = Counter(chunks['CHI'])
    #for chunk, count in child_counts.items():
    #    print("child used", chunk, count, "times.")

def clean(x):
    edited = []
    for i in x:
        if i == '.': #or '?' or '-': why doesnt this work????
            pass
        elif i == '?':
            pass
        elif i == '-':
            pass
        else:
            edited.append(i)
    return edited

def filterall(x):
    words  = []
    for i in x:
        words.append(i)
    for i in range (2,4):
        for j in range(0,len(words) - i + 1):
            if filtergrams(' '.join(map(str,words[j:j+i]))):
                return False
    return True;

def filterallold(x):
    valid = True;
    for i in x:
        valid = filter(i)
        if valid == False:
            print("False!")
            return False
    return True;

def filter(x):
    y = x.word
    try:
        return {
            'Tot Time': False,
            'Tot': False,
            'Time': False,
            'one two': False,
            'two three': False,
            'tweet tweet': False,
            'tweet': False,
            'uh oh': False,
            'oh oh': False,
            'J P': False,
            'um a': False,
            'um': False,
            'beep beep': False,
            'beep': False,
            'um um': False,
        }[y]
    except KeyError:
        return True
