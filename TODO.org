#+TITLE: Tasks and Questions
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

* Corpora
*** DONE Where are the previous implementation attempts?
    CLOSED: [2016-10-11 Tue 12:58]
    - State "DONE"       from "TODO"       [2016-10-11 Tue 12:58]
    :LOGBOOK:
    CLOCK: [2016-10-11 Tue 12:42]--[2016-10-11 Tue 12:58] =>  0:16
    :END:
***** Nick
      - larxtor/scripts/Corpus Analysis/
***** erin / paul BUCLD
      These are older and not well documented...
      - larxtor/scripts/old-projects/bucld/
      - larxtor/scripts/old-projects/erin-bu-data/
      - larxtor/scripts/old-projects/erin-bu-quick-n-dirty/
*** TODO What corpora are we actually going to use for this?
***** 2013 talk corpora
      - Brown Corpus: Eve
      - Bloom Corpus:
      - Manchester Corpus: 10 different children
***** 2014 talk corpora & Nick's version
      - valian corpus: 21 children
* TODO Test talkbank parser on the corpora of interest
  [[https://github.com/pfeyz/talkbank-parser/][Talkbank Parser]] is a python module for parsing the XML version of the
  [[http://talkbank.org/][Talkbank corpora]], though I've only used it on the [[http://childes.talkbank.org/][CHILDES]] subset. The XML
  files are available [[http://childes.psy.cmu.edu/data-xml/][here]].

  There might be structure or annotations that we didn't encounter in the
  corpora we parsed in the past. We need to make sure these cases are
  identified and handled correctly.

  I have some [[https://github.com/pfeyz/talkbank-parser/blob/master/talkbank_parser/tests.py][simple tests]] set up that we should probably extend.
