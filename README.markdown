# Ngram Formulae Analysis Project

## Overview

## Setup

- You'll probably want to install [virtualenv][] and [virtualenvwrapper][]. This
will allow you to keep the dependencies for different projects separate from
each other.

- create and activate the virtual environment

    $ mkvirtualenv formulae -p $(which python3)
    $ workon formulae  # activate virtualenv

this will create a sandboxed environment name formulae.

- install the python dependencies for the project

    $ pip install -r requirements.txt

[virtualenv]: https://virtualenv.pypa.io/en/latest/installation/
[virtualenvwrapper]: https://virtualenvwrapper.readthedocs.io/en/latest/

## Components

### Corpus Versioning
We need a way to ensure version consistency within our dataset, especially if we
end up using multiple corpora. The issuers of our data don't have a versioning
scheme and make periodic unannounced updates to the dataset.

### Corpus Parsing
- using talkbank parser to convert each xml doc to a list of parsed utterances.
- preserve as much annotation data as possible

### Utterance Filtering
Erin mentions the following as items that should be excluded from our analysis:

> compound nouns, direct repetitions (tweet tweet) and errors (New Investigator
> is counted as a bigram), but I don't think songs or routinized language should
> be counted here

### Formulae Extraction
- convert utterances to bigrams, trigrams etc
- should preserve speaker, file source, other metadata with each "chunk".

### Formulae Analysis
- computes statistics (dice coefficient, maybe other similarity measures) on the
  formulae chunks between speakers.
