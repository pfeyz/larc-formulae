from collections import OrderedDict

illegalgrams = {'New Investigator': False,
                'Tot Time': False,
                'one two': False,
                'two three': False,
                'tweet tweet': False,
                'uh oh': False,
                'oh oh': False,
                'J P': False,
                'um a': False,
                'beep beep': False,
                'um um': False,
                'night moon': False,
                'night night': False,
                'old MacDon': False,
                'old MacDon had': False,
                'MacDon had a': False,
                'one two three': False,
                'two three four': False,
                'three four five': False,
                'four five six': False,
                'five six seven': False,
                'six seven eight': False,
                'seven eight nine': False,
                'eight nine ten': False,
                'nine ten eleven': False,
                'MacDon had': False,
                'a b c': False,
                'd e f': False,
                'e f g': False,
                'e f g': False,
                'b c d': False,
                'c d e': False,
                'h i j': False,
                'f g h': False,
                'a b': False,
                'b c': False,
                'f g': False,
                'd e': False,
                'c d': False,
                'e f': False,
                'ring around': False,
                'ring around the': False,
                'around the rosey': False,
                'the rosey': False,
                'around the': False,
                'pop goes the': False,
                'quack quack quack': False}
def filtergrams(x):
    formatted = x[0]
    for i in range(1, len(x)):
        formatted += " "
        formatted += x[i]
    if formatted in illegalgrams:
        return False
    return True;

##returns list of tuples of ngrams
def createngramsold(text, n):
    ngrams = []
    if n == 1:
        ngrams = text
    else:
        if filtergrams(text[i:i+n]):
            ngrams.append(tuple(text[i:i+n]))
    return ngrams

def createngrams(text, n):
    convert = []
    for i in text:
        convert.append(i.word)
    ngrams = []
    if n == 1:
        ngrams = convert
    else:
        for i in range(len(convert) - n + 1):
            if filtergrams(convert[i:i+n]):
                ngrams.append(tuple(convert[i:i+n]))
    return ngrams

# returns unordered dictionary of keys=ngrams and values=frequencies
def freqdict(ngrams):
    freqdict = {}
    for gram in ngrams:
        gramtup = gram
        try:
            freqdict[gramtup] += 1
        except KeyError:
            freqdict[gramtup] = 1
    return freqdict

## returns ordered dictionary of ngrams by frequencies
def freqdictordered(ngrams):
    order = {}
    order = freqdict(ngrams)
    #creates ordered dictionary sorted by frequency value
    order = OrderedDict(sorted(order.items(), key = lambda x: x[1], reverse=True))
    #for b in sorted(freqdict, key=freqdict.get, reverse=True):
    #orderedfreq.append([b, freqdict[b]])
    return order


## returns list of top x frequent ngrams, plus those that tie with that last value
## works with ordered dictionary
def topxfreqdict(ngrams, x):
    orderfreq = freqdictordered(ngrams)
    listversion = list(orderfreq.items())
    topx = []
    length = x
    if len(listversion) < x:
        length = len(listversion)
    for i in range(length):
        topx.append(listversion[i])
    for i in range(length, len(listversion)):
        if listversion[i-1][1] == 1:
            break
        if listversion[i-1][1] == listversion[i][1]:
            topx.append(listversion[i])
        else:
            break
    return topx


## returns list of top x frequent bigrams, and their frequencies
## also includes bigrams with equal frequencies to xth most frequent ngram
## work with list
def topxfreqstring(ngrams, x):
    orderfreq = gramfreq(ngrams)
    topx = []
    for i in range(x):
        topx.append(orderfreq[i])
    for i in range(x, len(orderfreq)-1):
        if orderfreq[i-1][1] == orderfreq[i+1][1]:
            print(orderfreq[i][1])
            topx.append(orderfreq[i])
        else:
            break
    return topx
