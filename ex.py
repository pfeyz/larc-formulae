import os
import xlwt
import openpyxl
from openpyxl.styles import Font
import xlsxwriter
from qmain import*

'''
Excel Styling
'''
wrap_bold = 'font: bold 1; align: wrap 1;'
style = xlwt.easyxf(wrap_bold)
style1 = xlwt.easyxf('pattern: pattern solid, fore_colour black;')
unique = xlwt.easyxf('pattern: pattern solid, fore_colour yellow;')



def newWorkbook():

    new = xlwt.Workbook(encoding="utf-8")

    sheet1 = new.add_sheet("Sheet 1 - Bigrams")
    sheet1.write(0, 0, "Phase (child)", style) # maybe a heading?

    sheet1.write(0, 1, "Child Bigram", style)
    sheet1.write(0, 2, "Child Bigram Raw Frequency", style)
    sheet1.write(0, 3, "Raw Frequency of Child's Bigram in Adult's Speech", style)
    sheet1.write(0, 4, "Total Bigram Tokens (Child)", style) # maybe not here
    sheet1.write(0, 5, "Percent of Total Tokens in Child's Top 10", style) # maybe not here
    sheet1.write(0, 7, "Adult Bigram", style)
    sheet1.write(0, 8, "Adult Bigram Raw Frequency", style)
    sheet1.write(0, 9, "Raw Frequency of Adult's Bigram in Child's Speech", style)
    sheet1.write(0, 10, "Total Bigram Tokens (Adult)", style) # maybe not here
    sheet1.write(0, 11, "Percent of Total Tokens in Adult top 10", style) # maybe not here

    sheet2 = new.add_sheet("Sheet 2 - Trigrams")
    sheet2.write(0, 0, "Phase (child)", style) # maybe a heading?

    sheet2.write(0, 1, "Child Trigram", style)
    sheet2.write(0, 2, "Child Trigram Raw Frequency", style)
    sheet2.write(0, 3, "Raw Frequency of Child's Trigram in Adult's Speech", style)
    sheet2.write(0, 4, "Total Trigram Tokens (Child)", style) # maybe not here
    sheet2.write(0, 5, "Percent of Total Tokens in Child's Top 10", style) # maybe not here
    sheet2.write(0, 7, "Adult Trigram", style)
    sheet2.write(0, 8, "Adult Trigram Raw Frequency", style)
    sheet2.write(0, 9, "Raw Frequency of Adult's Trigram in Child's Speech", style)
    sheet2.write(0, 10, "Total Trigram Tokens (Adult)", style) # maybe not here
    sheet2.write(0, 11, "Percent of Total Tokens in Adult top 10", style) # maybe not here

    return new, sheet1, sheet2


def writetoexcel(workbook, sheet, startingrow, phase, childtopx, adulttopx, childtotal, adulttotal, childgrams, adultgrams, combine, child):
    #format1 = workbook.add_format({'bg_color': '#edf25e'})
    compared_top = [x[0] for x in adulttopx]
    comparing = [x[0] for x in childtopx]

    total_count = 0;
    #writes all the child ngram data
    for i in range(0, len(childtopx)):
        sheet.write(startingrow+i, 0, phase)
        if childtopx[i][0] in compared_top:
            sheet.write(startingrow+i, 1, ' '.join(map(str,childtopx[i][0])),unique)
        else:
            sheet.write(startingrow+i, 1, ' '.join(map(str,childtopx[i][0])))
        sheet.write(startingrow+i, 2, childtopx[i][1])
        #sheet['B'+ str(startingrow+i)].font = Font(bold=True)
            #sheet.conditional_format(get_column_letter('B'+ str(startingrow+i)), {'type':     'text', 'format':   format1})

        count = 0
        for j in adultgrams:
            if childtopx[i][0] == j:
                count += 1
        sheet.write(startingrow+i, 3, count)
        total_count += count

        #when on the last element, compute totals and calculate percent
        if i == len(childtopx)-1: ## can be a finally sort of loop
            sheet.write(startingrow+i+1,4,childtotal, style)

            topxchildtotal = 0
            topxchildcomp = 0
            for ngram in childtopx:
                topxchildtotal += ngram[1]
            sheet.write(startingrow+i+1,5,round(float(topxchildtotal)/float(childtotal),2), style)

            sheet.write(startingrow+i+1, 1, "Total:", style)
            sheet.write(startingrow+i+1, 2, topxchildtotal, style)
            sheet.write(startingrow+i+1, 3, total_count, style)

    total_count = 0
    #writes all the adult ngram data
    for i in range(0, len(adulttopx)):
        if adulttopx[i][0] in comparing:
            sheet.write(startingrow+i, 7, ' '.join(map(str,adulttopx[i][0])),unique)
        else:
            sheet.write(startingrow+i, 7, ' '.join(map(str,adulttopx[i][0])))
        sheet.write(startingrow+i, 8, adulttopx[i][1])
        count = 0
        for j in childgrams:
            if adulttopx[i][0] == j:
                count += 1
        sheet.write(startingrow+i, 9, count)
        total_count += count

        #when on the last element, compute totals and calculate percent
        if i == len(adulttopx)-1:
            sheet.write(startingrow+i+1,10,adulttotal, style)

            topxadulttotal = 0
            for ngram in adulttopx:
                topxadulttotal += ngram[1]
            sheet.write(startingrow+i+1,11,round(float(topxadulttotal)/float(adulttotal),2), style)
            sheet.write(startingrow+i+1, 7, "Total:", style)
            sheet.write(startingrow+i+1, 8, topxadulttotal, style)
            sheet.write(startingrow+i+1, 9, total_count, style)
    if combine:
        workbook.save(child+ "_combined_ngrams.xls")
    else:
        workbook.save(child+ "_ngrams.xls")
    return


def analyzengrams2(output_file, directory, ngram_len, sheet, combine_phases, child):

    currentrow = 1 #tracks numbed of already-used rows in sheet1

    i = 0
    while i < len(directory)-1: #for each file in folder
        '''''''''
        Variables
        '''''''''
        allchildngrams = []
        alladultngrams = []

        numchildngrams = 0 # why am i using this
        numadultngrams = 0

        if combine_phases:
            phase = directory[i].split(".")[0][0:-1]
        else:
            phase = directory[i].split(".")[0]
        print(phase)

        name = os.path.join('Manchester',child, directory[i])

        mortokendict = generated(name, ngram_len, {})

        if combine_phases:
            for j in range(i, len(directory)-1):
                  if directory[j].split(".")[0][0:-1] == directory[j+1].split(".")[0][0:-1]:
                      nextname = os.path.join('Manchester',child, directory[j+1])
                      mortokendict = generated(nextname, ngram_len, mortokendict)
                  else:
                      break
            i = j
        #print(mortokendict2.keys())
        allchildngrams = mortokendict['CHI']

        # what if there is multiple? something else?
        if 'MOT' in mortokendict:
            alladultngrams = mortokendict['MOT']
        elif 'FAT' in mortokendict:
            alladultbigrams = mortokendict['FAT']
        else:
            alladultbigrams = mortokendict['DAD']

        numchildngrams = len(allchildngrams)
        numadultngrams = len(alladultngrams)


        topchildngrams = topxfreqdict(allchildngrams, 10)
        topadultngrams = topxfreqdict(alladultngrams, 10)

        writetoexcel(excelfile, sheet, currentrow, phase, topchildngrams, topadultngrams, numchildngrams, numadultngrams, allchildngrams, alladultngrams, combine_phases, child)

        if len(topchildngrams) >= len(topadultngrams):
            currentrow += len(topchildngrams)+5
        else:
            currentrow += len(topadultngrams)+5
        i += 1

    if combine_phases:
        output_file.save(child+ "_combined_ngrams.xls")
    else:
        output_file.save(child + "_ngrams.xls")


directory = os.listdir("Manchester") #directory should be a folder of text files only and must be in the current working directory
for person in directory:
    inner_directory = os.listdir("Manchester/"+ person)
    excelfile,sheet1,sheet2 = newWorkbook()
    analyzengrams2(excelfile, inner_directory, 2, sheet1, True, person)
    analyzengrams2(excelfile, inner_directory, 3, sheet2, True, person)
